using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffUIDrawer : MonoBehaviour
{
    public BuffContainer BuffContainer;
    public GameObject GameIconPrefab;
    public BuffCategory Category;
    public GameObject Parent;
    private List<BuffIcon> BuffIcons = new List<BuffIcon>();

    private List<BuffBase> AttachedBuffs = new List<BuffBase>();

    private Dictionary<BuffBase, BuffIcon> AttachedBuffDictionary = new Dictionary<BuffBase, BuffIcon>();

    void Start()
    {

        BuffContainer.OnAttached += OnBuffAttached;
        BuffContainer.OnDetached += OnBuffDetached;
    }
    private void OnDestroy()
    {
        BuffContainer.OnAttached -= OnBuffAttached;
        BuffContainer.OnDetached -= OnBuffDetached;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (var item in AttachedBuffDictionary)
        {
            BuffBase b = item.Key;
            BuffIcon icon = AttachedBuffDictionary[b];
            icon.CurrentTime = b.CurrentTime;
        }
    }

    private void OnBuffAttached(BuffBase buff)
    {
        if (buff.BuffInfo.Category != Category)
            return;

        BuffIcon icon = Instantiate(GameIconPrefab, Parent.transform).GetComponent<BuffIcon>();
        if (icon == null) return;

        icon.MaxTime = TimeSpan.FromSeconds(buff.BuffTime);
        icon.CurrentTime = buff.CurrentTime;
        icon.Icon = buff.BuffInfo.Icon;

        AttachedBuffDictionary[buff] = icon;
    }


    private void OnBuffDetached(BuffBase buff)
    {
        if (buff.BuffInfo.Category != Category)
            return;
        BuffIcon icon = AttachedBuffDictionary[buff];
        Destroy(icon.gameObject);
        AttachedBuffDictionary.Remove(buff);
    }

}
