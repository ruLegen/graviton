using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    private string aliveTemplate = "Alive {0}/{1}";
    private string scoreTemplate = "Score: {0}";

    [SerializeField] private Text ScoreText;
    [SerializeField] private Text AliveText;

    public int AliveCount
    {
        get => aliveCount;
        set
        {
            aliveCount = value;
            AliveText.text = string.Format(aliveTemplate, AliveCount, MaxAliveCount);
        } 
    }
    public int MaxAliveCount 
    { 
        get => maxAliveCount;
        set
        {
            maxAliveCount = value;
            AliveText.text = string.Format(aliveTemplate, AliveCount, MaxAliveCount);
        }
    }
    public int Score
    {
        get => score;
        set
        {
            score = value;
            ScoreText.text = string.Format(scoreTemplate, score);
        }
    }


    private int aliveCount;
    private int maxAliveCount;
    private int score;

}
