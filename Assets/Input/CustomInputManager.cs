using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CustomInputManager : MonoBehaviour
{
    /*
     * @brief returns float [-1,1] that indicate jump dirrection, used when play with joystick
     */
    public UnityAction<float> OnJump;
    /*
    * @brief indicates jump dirrection changed, used when play with keyboard
    */
    public UnityAction OnJumpToggle;
    /*
     * @brief returns float [-1,1] that indicate move dirrection, used when play with joystick or  keyboard
     */
    public UnityAction<float> OnMove;
    /*
     * @brief returns Vector2 that indicates shoot dirrection, used when play with joystick or  keyboard
     */
    public UnityAction<Vector2> OnFire;


    public CustomJoystick MoveJoystick;
    public CustomJoystick FireJoystick;

    public bool UseJoysticks = true;

    private bool shouldTriggerJump = true;
    private float prevSign = 0;
    void Start()
    {
        MoveJoystick.OnPointerUpEvent += MovePointerUp;
        FireJoystick.OnPointerUpEvent += FirePointerUp;
        Debug.LogWarning("Using Device: " + (UseJoysticks ? "Joystick" : "Keyboard"));
    }


    void Update()
    {
        if (UseJoysticks)
        {
            UpdateJoystic();
        }
        else
        {
            UpdateKeyboard();
        }
    }

    private void UpdateJoystic()
    { // Joystick handler
        if (MoveJoystick.Direction != Vector2.zero)
        {
            if (Mathf.Abs(MoveJoystick.Vertical) > 0.15 && shouldTriggerJump)       // 0.15 minimum threashold needs to trigger event
            {
                shouldTriggerJump = false;
                OnJump?.Invoke(Mathf.Sign(MoveJoystick.Vertical));
            }
            if (prevSign != Mathf.Sign(MoveJoystick.Vertical))
                shouldTriggerJump = true;

            prevSign = Mathf.Sign(MoveJoystick.Vertical);

            OnMove?.Invoke(MoveJoystick.Horizontal);
        }
        else
        {
            prevSign = 0;
        }

        if(FireJoystick.Direction != Vector2.zero)
        {
            OnFire.Invoke(FireJoystick.Direction);
        }
    }


    private void UpdateKeyboard()
    {
        // Keyboard Handler
        var moveDirection = Input.GetAxis("Horizontal");
        OnMove?.Invoke(moveDirection);
        if (Input.GetButtonDown("Jump"))
        {
            OnJumpToggle?.Invoke();
        }
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Vector3 mousePos = Input.mousePosition;
            mousePos.z = Camera.main.nearClipPlane;
            var worldPos = Camera.main.ScreenToWorldPoint(mousePos);
            OnFire.Invoke((worldPos - transform.root.transform.position));
        }
    }

    private void FirePointerUp()
    {
    }

    private void MovePointerUp()
    {
        OnMove?.Invoke(0);
    }
}
