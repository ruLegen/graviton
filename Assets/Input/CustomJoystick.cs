using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CustomJoystick : FloatingJoystick
{
    public UnityAction OnPointerUpEvent;
    public UnityAction OnPointerDownEvent;

    public override void OnPointerDown(PointerEventData eventData)
    {
        OnPointerDownEvent?.Invoke();
        base.OnPointerDown(eventData);
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        OnPointerUpEvent?.Invoke();
        base.OnPointerUp(eventData);
    }

}
