using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class BuffContainer : MonoBehaviour
{
    public UnityAction<BuffBase> OnAttached;
    public UnityAction<BuffBase> OnDetached;

    private Dictionary<BuffCategory, BuffBase> _buffContainer = new Dictionary<BuffCategory, BuffBase>();

    private void FixedUpdate()
    {
        BuffBase[] arr = new BuffBase[_buffContainer.Count];
        _buffContainer.Values.CopyTo(arr, 0);

        foreach (var item in arr)
        {
            item.Update(Time.fixedDeltaTime * 1000);
        }
    }
    public void Attach(BuffBase buffBase)
    {
        BuffCategory category = buffBase.BuffInfo.Category;

        var hasAlreadyAttached = _buffContainer.ContainsKey(category);
        if(!hasAlreadyAttached)
        {
            _Attach(buffBase);
        }
        else
        {
            var prevBuff = _buffContainer[category];
            if (buffBase.GetType() == prevBuff.GetType())
            {
                prevBuff.Stacked(buffBase);
            }
            else
            {
                Detach(prevBuff);                       // detach previous one
                _Attach(buffBase);                                      // attach new;
            }
        }
    }

    private void _Attach(BuffBase buffBase)
    {
        _buffContainer[buffBase.BuffInfo.Category] = buffBase;
        buffBase.OnExpired += BuffBase_OnExpired;
        buffBase.Attached(gameObject);
        OnAttached?.Invoke(buffBase);
    }

    private void BuffBase_OnExpired(BuffBase buff)
    {
        Detach(buff);
    }

    public void Detach(BuffBase buffBase)
    {
        _buffContainer.Remove(buffBase.BuffInfo.Category);
        OnDetached?.Invoke(buffBase);
        buffBase.Dettached(gameObject);
    }
}
