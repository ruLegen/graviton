using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuffSupplier : MonoBehaviour
{
    public SOBuffBase[] buffArray;
    public GameObject PickupEffect;
    public AudioClip PickupSound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var buffContainer = collision.attachedRigidbody.GetComponent<BuffContainer>();
        if (!buffContainer) return;
        foreach (var buff in buffArray)
        {
            buffContainer.Attach(buff.Instance());
        }
        AudioSource.PlayClipAtPoint(PickupSound, transform.position);
        var pickup = Instantiate(PickupEffect, transform.position, Quaternion.identity);
        Destroy(pickup, 1);
        Destroy(gameObject);
    }

}

