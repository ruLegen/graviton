using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]

public abstract class BuffBase
{
    public event UnityAction<BuffBase> OnExpired;

    public SOBuffBase BuffInfo;
    public TimeSpan CurrentTime 
    {
        get => buffTimer != null ? buffTimer.RemainTime : TimeSpan.FromSeconds(0); 
    }
    public float BuffTime;

    protected Timer buffTimer;

    protected GameObject AttachedTo = null;
    protected bool IsAttached = false;

    // Update should be called once per fixed update tick
    public virtual void Update(float dealtaTime) 
    {
        if (IsAttached)
        {
            buffTimer.Update(dealtaTime);
        }
    }

    // Implement main logic here
    public virtual void Attached(GameObject gameObject) 
    {
        AttachedTo = gameObject;
        IsAttached = true;
    }

    // Implement Debuf effect when buff detaches
    public virtual void Dettached(GameObject gameObject) 
    {
        AttachedTo = null;
        IsAttached = false;
    }

    // Implement logic when the same buff has been attached
    // maybe reset timer or add some values
    // here you should check and cast to specific buff
    public virtual void Stacked(BuffBase gameObject) { }

    protected void InitTimer()
    {
        buffTimer = new Timer(TimeSpan.FromSeconds(BuffTime));
        buffTimer.OnCountDown += BuffTimer_OnCountDown;
        buffTimer.Start();
    }

    protected virtual void BuffTimer_OnCountDown()
    {
        Expire();
    }
    // Call when Buff should be detached
    protected void Expire()
    {
        OnExpired?.Invoke(this);
    }
}
