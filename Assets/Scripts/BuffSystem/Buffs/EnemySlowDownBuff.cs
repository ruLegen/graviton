using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlowDownBuff : BuffBase
{
    private EnemyAI[] enemies;
    private float slowDownRate;

    public EnemySlowDownBuff(SOEnemySlowDownBuff buff)
    {
        BuffInfo = buff;
        BuffTime = buff.BuffTime;
        this.slowDownRate = buff.NewSpeedRate;
    }

    public override void Attached(GameObject gameObject)
    {
        base.Attached(gameObject);
        InitTimer();

        enemies = GameObject.FindObjectsOfType<EnemyAI>();
        if (enemies != null)
        {
            SlowDown(enemies);
        }
    }


    public override void Dettached(GameObject gameObject)
    {
        base.Dettached(gameObject);
        SetDefaultSpeed(enemies);
    }

    public override void Stacked(BuffBase gameObject)
    {
        /*
         * The problem is when Stacked occured, there is maybe new enemies been spawned
         * Therefore we should reset speed to all previous capturedEnemies
         * And recapture new enemies
         */
        base.Stacked(gameObject);
        if (gameObject is EnemySlowDownBuff)
        {
            InitTimer();
            SetDefaultSpeed(enemies);
            enemies = GameObject.FindObjectsOfType<EnemyAI>();
            SlowDown(enemies);
        }
    }

    private void SlowDown(EnemyAI[] enemies)
    {
        if (enemies == null) return;

        foreach (var enemy in enemies)
        {
            if (enemy != null)
                enemy.CurrentSpeed *= slowDownRate;
        }
    }

    private void SetDefaultSpeed(EnemyAI[] enemies)
    {
        if (enemies == null) return;

        foreach (var enemy in enemies)
        {
            enemy.CurrentSpeed = enemy.InitialSpeed;
        }
    }
}
