using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmortalBuff : BuffBase
{
    private HealthComponent _healthComponent;
    private float _storedDamageMultiplier;

    public ImmortalBuff(SOImmortalBuff buff)
    {
        BuffInfo = buff;
        BuffTime = buff.BuffTime;
    }
  
    public override void Update(float deltaTime)
    {
        if (IsAttached)
        {
            buffTimer.Update(deltaTime);
        }
    }
    public override void Attached(GameObject gameObject)
    {
        base.Attached(gameObject);
        InitTimer();

        var healthComponent = gameObject.GetComponent<HealthComponent>();
        if (healthComponent == null)
        {
            buffTimer.Stop();
            Expire();
            return;
        }
        else
        {
            _healthComponent = healthComponent;
        }

        _storedDamageMultiplier = _healthComponent.DamageMultiplyer;
        _healthComponent.DamageMultiplyer = 0;
    }

    public override void Dettached(GameObject gameObject)
    {
        base.Dettached(gameObject);

        if (_healthComponent != null)
            _healthComponent.DamageMultiplyer = _storedDamageMultiplier;
    }

    public override void Stacked(BuffBase buff)
    {
        base.Stacked(buff);
        if (buff.GetType() == typeof(ImmortalBuff))
        {
            InitTimer();
            _healthComponent.DamageMultiplyer = 0;
        }
    }


}
