using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponDamageBuff : BuffBase
{
    public float Damage = 1f;
    private float _storedDamage = 10;
    private WeaponComponent _weaponCompent;

    public WeaponDamageBuff(SOWeaponDamageBuff buff)
    {
        BuffInfo = buff;
        BuffTime = buff.BuffTime;
        Damage = buff.Damage;
    }
  

    public override void Attached(GameObject gameObject)
    {
        base.Attached(gameObject);
        InitTimer();

        var weaponComponent = gameObject.GetComponent<WeaponComponent>();
        if (weaponComponent == null)
        {
            buffTimer.Stop();
            Expire();
            return;
        }
        else
        {
            _weaponCompent = weaponComponent;
        }

        _storedDamage = _weaponCompent.ProjectTileDamage;
        _weaponCompent.ProjectTileDamage = Damage;
    }

    public override void Dettached(GameObject gameObject)
    {
        base.Dettached(gameObject);

        if (_weaponCompent != null)
            _weaponCompent.ProjectTileDamage = _storedDamage;
    }

    public override void Stacked(BuffBase buff)
    {
        base.Stacked(buff);
        if (buff.GetType() == typeof(WeaponDamageBuff))
        {
            InitTimer();
            _weaponCompent.ProjectTileDamage  += (buff as WeaponDamageBuff).Damage;
        }
    }

   
}
