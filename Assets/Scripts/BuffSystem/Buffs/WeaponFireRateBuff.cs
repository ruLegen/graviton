using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WeaponFireRateBuff : BuffBase
{
    public float BuffShootDelay = 0.1f;
    private float _storedShootDelay = 1;

    private WeaponComponent _weaponCompent;

    public WeaponFireRateBuff(SOFireRateBuff buff)
    {
        BuffInfo = buff;
        BuffTime = buff.BuffTime;
        BuffShootDelay = buff.BuffShootDelay;
    }

    public override void Attached(GameObject gameObject)
    {
        base.Attached(gameObject);

        InitTimer();
        
        var weaponComponent = gameObject.GetComponent<WeaponComponent>();
        if(weaponComponent == null)
        {
            buffTimer.Stop();
            Expire();
            return;
        }else
        {
            _weaponCompent = weaponComponent;
        }

        _storedShootDelay = _weaponCompent.ShootDelay;
        _weaponCompent.ShootDelay = BuffShootDelay;
    }

    /*
        At this point:
        AttachedTo = null;
        IsAttached = false;
     */
    public override void Dettached(GameObject gameObject)
    {
        Debug.Log("Detached");
        base.Dettached(gameObject);

        if(_weaponCompent != null)
            _weaponCompent.ShootDelay = _storedShootDelay;
    }

    public override void Stacked(BuffBase buff)
    {
        if(buff.GetType() == typeof(WeaponFireRateBuff))
        {
            Debug.Log("Stacked");

            InitTimer();
            _weaponCompent.ShootDelay = (buff as WeaponFireRateBuff).BuffShootDelay;
        }
    }

}
