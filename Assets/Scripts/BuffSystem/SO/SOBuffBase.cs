using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * This class is base for all Scriptable Objects that hold Buff Data
 * All classes that derived from this responsoble for creating instance of any BuffBase derived class
 * This is done because Unity does not allow select plain c# classes in inspector,
 * instead user put ScriptableObject that later creates ones
 */
public abstract class SOBuffBase : ScriptableObject
{
    public Sprite Icon;
    public abstract BuffBase Instance();
    public BuffCategory Category;
}
