﻿

using UnityEngine;


[CreateAssetMenu(fileName = "SOEnemySlowDownBuff", menuName = "Buff/EnemySlowDown")]
public class SOEnemySlowDownBuff : SOBuffBase
{
    public float BuffTime = 3;
    public float NewSpeedRate;
    public override BuffBase Instance()
    {
        return new EnemySlowDownBuff(this);
    }
}
