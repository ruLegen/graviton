using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "SOWeaponFireRate",menuName ="Buff/FireRate")]
public class SOFireRateBuff : SOBuffBase
{
    public float BuffTime = 3;
    public float BuffShootDelay = 0.1f;
    public override BuffBase Instance()
    {
        return new WeaponFireRateBuff(this);
    }
}
