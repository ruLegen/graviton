using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SOImmortalBuff", menuName = "Buff/Immortal")]

public class SOImmortalBuff : SOBuffBase
{
    public float BuffTime = 10;
    public override BuffBase Instance()
    {
        return new ImmortalBuff(this);
    }
}
