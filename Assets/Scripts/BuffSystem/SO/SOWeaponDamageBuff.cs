using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName ="SOWeaponDamage", menuName ="Buff/WeaponDamage")]
public class SOWeaponDamageBuff : SOBuffBase
{
    public float BuffTime = 3;
    public float Damage = 50f;
    public override BuffBase Instance()
    {
        return new WeaponDamageBuff(this);
    }
}
