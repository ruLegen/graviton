using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;

public class EnemyControllerComponent : MonoBehaviour
{
    public HUDController HUDController;
    public GameObject EnemyPrefab;

    public GameObject[] BuffDropArray;
    public float DropChance = 0.1f;
    public float MinEnemySpeed = 50;
    public float MaxEnemySpeed = 150;
    public float respawnDelay = 3f;
    public int MaxAliveEnemy = 3;
    public int MaxEnemiesOnLevel = 30;
    public int KillsForNextLevel = 5;
    public int AdditionCountToNextLevel = 3;


    public int AliveCount { get => AliveEnemies.Count; }
    public int ScoreForEnemy = 100;

    public bool isSpawnOnTileGenerated = true;
    public TileMapGenerator TileMapGenerator;

    private List<GameObject> AliveEnemies = new List<GameObject>();


    private int killedEnemyCount;

    private AstarPath pathFinder;
    private GridGraph enviromentData;

    private Timer respawnTimer;
    private GraphNode[] walkableNodes;
    private System.Random random;
    private int score = 0;


    // Start is called before the first frame update
    void Start()
    {
        TileMapGenerator.TileGenerated += TileMapGenerator_TileGenerated;
        Debug.Log("Registered");

        if (TileMapGenerator == null)
            isSpawnOnTileGenerated = false;
        random = new System.Random((int)(DateTime.Now - new DateTime(0)).TotalSeconds);

        pathFinder = GetComponent<AstarPath>();
        AstarPath.OnLatePostScan += OnGraphScanned;

        respawnTimer = new Timer(TimeSpan.FromSeconds(respawnDelay));
        respawnTimer.OnCountDown += RespawnTimer_OnCountDown;
        respawnTimer.Start();
        
        UpdateUI();
    }

   

    private void TileMapGenerator_TileGenerated()
    {
        pathFinder.Scan();
    }
    private void OnGraphScanned(AstarPath script)
    {
        if(script == pathFinder)
        {
            enviromentData = pathFinder.data.gridGraph;
            InitNodes();
            if (isSpawnOnTileGenerated)
            {
                SpawnEnemy();
            }
        }
    }

    private void InitNodes()
    {
        List<GraphNode> nodes = new List<GraphNode>();
        enviromentData.GetNodes((n) =>
        {
            if (n.Walkable)
                nodes.Add(n);
        });
        walkableNodes = nodes.ToArray();
    }



    // Update is called once per frame
    void FixedUpdate()
    {
        respawnTimer.Update(Time.fixedDeltaTime * 1000);
    }

    private void RespawnTimer_OnCountDown()
    {
        respawnTimer.Start();
        SpawnEnemy();
    }

    private void SpawnEnemy()
    {
        if (AliveCount == MaxAliveEnemy)
        {
            return;
        }
        int respawnCount = (int)(MaxAliveEnemy - AliveCount);
        for (int i =0; i< respawnCount; i++)
        {
            int index = random.Next(0, walkableNodes.Length);
            var spawnNode = walkableNodes[index];

            var enemy = GameObject.Instantiate(EnemyPrefab, (Vector3)spawnNode.position, Quaternion.identity);
            AliveEnemies.Add(enemy);
            enemy.GetComponent<Rigidbody2D>().gravityScale = random.Next(0, 2) % 2 == 0 ? -1 : 1;
            enemy.GetComponent<HealthComponent>().OnDie += Enemy_OnDie;
            var enemyAI = enemy.GetComponent<EnemyAI>();
            if(enemyAI)
            {
                float newSpeed = (float)(random.NextDouble() * (MaxEnemySpeed - MinEnemySpeed) + MinEnemySpeed);
                enemyAI.InitialSpeed = newSpeed;
            }
        }
        UpdateUI();

    }



    private void Enemy_OnDie(GameObject self, GameObject killer)
    {
        AliveEnemies.Remove(self);
        self.GetComponent<HealthComponent>().OnDie -= Enemy_OnDie;
        killedEnemyCount++;

        if(killedEnemyCount % KillsForNextLevel == 0)
        {
            MaxAliveEnemy = Mathf.Clamp(MaxAliveEnemy+ AdditionCountToNextLevel,0, MaxEnemiesOnLevel);
        }
        score += ScoreForEnemy;
        UpdateUI();

        if (random.NextDouble() < DropChance)
        {
            int index = random.Next(0, BuffDropArray.Length);
            var obj = Instantiate(BuffDropArray[index], self.transform.position, Quaternion.identity);
        }
}

    private void UpdateUI()
    {
        HUDController.AliveCount = AliveCount;
        HUDController.MaxAliveCount = MaxAliveEnemy;
        HUDController.Score = score;
    }
}
