using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthComponent : MonoBehaviour
{
    public delegate void DamageTaken(DamageInfo damageInfo);
    public event DamageTaken OnDamageTaken;
    // 1-st GameObject = Enemy;
    // 2-nd GameObject = Attacker;
    public event Action<GameObject,GameObject> OnDie;

    public bool IsDead { get => _currentHealth == 0; }
    public float DamageMultiplyer = 1;

    [SerializeField] private float _maxHealth = 100;
    private float _currentHealth = 0;
    void Start()
    {
        _currentHealth = _maxHealth;
    }

    // TODO PassHere DamageStruct with
    // damage value itself and who applyed the damage
    public void ApplyDamage(DamageInfo damageInfo)
    {
        if (IsDead)
            return;

        _currentHealth = Mathf.Clamp(_currentHealth - damageInfo.Damage * DamageMultiplyer, 0,_maxHealth);
        OnDamageTaken?.Invoke(damageInfo);
        if (IsDead)
            OnDie?.Invoke(gameObject,damageInfo.Attacker);
    }
    public void AddHealth(float health)
    {
        _currentHealth = Mathf.Clamp(_currentHealth - health, 0, _maxHealth);
    }
    public void Kill(GameObject Attacker)
    {
        ApplyDamage(new DamageInfo(Attacker , _currentHealth));
    }
}
