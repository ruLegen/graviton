using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuLevelController : MonoBehaviour
{
    // Start is called before the first frame update
    public void LoadTutorial()
    {
        SceneManager.LoadScene(2);
    }
    public void LoadPlayLevel()
    {
        SceneManager.LoadScene(1);
    }
    public void LoadMainMenuLevel()
    {
        SceneManager.LoadScene(0);
    }
    public void ExitGame()
    {
        Application.Quit();
    }
}
