using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour
{
    public float Damage = 50;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        HealthComponent healthComponent = collision.gameObject.GetComponent<HealthComponent>();
        if(healthComponent && collision.gameObject.tag != "Enemy")
        {
            healthComponent.ApplyDamage(new DamageInfo(gameObject, Damage));
        }
    }

}
