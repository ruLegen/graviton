using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowController : MonoBehaviour
{
    public Transform FollowObject;
    private Camera _currentCamera;
    private Vector3 _cameraBoxSize;
    private float LeftBound
    {
        get => (_currentCamera.transform.position - _cameraBoxSize / 2).x;
    }
    private float RightBound
    {
        get => (_currentCamera.transform.position + _cameraBoxSize / 2).x;
    }
    private float TopBound
    {
        get => (_currentCamera.transform.position + _cameraBoxSize/2  ).y;
    }
    private float BottomBound
    {
        get => (_currentCamera.transform.position - _cameraBoxSize/2 ).y;
    }
    void Start()
    {
         _currentCamera = Camera.main;
    }


    // Update is called once per frame
    void Update()
    {
        if(FollowObject.position.x < LeftBound)
        {
            _currentCamera.transform.position -= new Vector3(_cameraBoxSize.x , 0, 0);
        }
        if (FollowObject.position.x > RightBound)
        {
            _currentCamera.transform.position += new Vector3(_cameraBoxSize.x, 0, 0);
        }
        if (FollowObject.position.y < BottomBound)
        {
            _currentCamera.transform.position -= new Vector3(0, _cameraBoxSize.y, 0);
        }
        if (FollowObject.position.y > TopBound)
        {
            _currentCamera.transform.position += new Vector3(0, _cameraBoxSize.y, 0);
        }
    }

    private void LateUpdate()
    {
        float screenAspect = (float)Screen.width / (float)Screen.height;
        float cameraHeight = _currentCamera.orthographicSize * 2;
        _cameraBoxSize = new Vector3(cameraHeight * screenAspect, cameraHeight, 0);
    }

}
