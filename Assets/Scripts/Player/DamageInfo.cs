﻿using System;
using UnityEngine;

    public struct DamageInfo
    {
        public GameObject Attacker;
        public float Damage;

        public DamageInfo(GameObject attacker, float damage)
        {
            Attacker = attacker;
            Damage = damage;
        }

    public override string ToString()
    {
        return $"Attacker: {Attacker.name}; Damage: {Damage}";
    }
}
