using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;
using System;

public class EnemyAI : MonoBehaviour
{
    public bool IsUpsideDown { get => rigidbody?.gravityScale < 0; }
    public bool IsTurningLeft { get => rigidbody?.velocity.x < 0; }
    public bool ShoudDrawPoint { get; private set; }
    public Vector3 DrawPoint { get; private set; }
    public bool IsUsePathFinding { get; private set; }
    
    public float InitialSpeed = 10;
    public float CurrentSpeed = 10;
    public float PursuitTime = 3;
    public string PursuitTag = "Player";
    public float SeeDistance = 3;
    public LayerMask RaycastLayerMask;

    private Rigidbody2D rigidbody;
    private Transform target;
    private HealthComponent healthComponent;
    private float walkXDirrection = 1;

    private Seeker seeker;
    private bool isPursuit = false;

    private Timer pursuitTimer;
    private Timer randomFlipTimer;
    private Vector3[] PathToTarget;
    private List<Vector3> ProjectPoints = new List<Vector3>();
    private int closestPoint;

    void Start()
    {
        CurrentSpeed = InitialSpeed;
        rigidbody = GetComponent<Rigidbody2D>();
        healthComponent = GetComponent<HealthComponent>();
        healthComponent.OnDie += HealthComponent_OnDie;
        healthComponent.OnDamageTaken += HealthComponent_OnDamageTaken;
        pursuitTimer = new Timer(TimeSpan.FromSeconds(PursuitTime));
        pursuitTimer.OnCountDown += PursuitTimer_OnCountDown;

        randomFlipTimer = new Timer(TimeSpan.FromSeconds(1));
        randomFlipTimer.OnCountDown += PursuitTimer_OnCountDown1;
        randomFlipTimer.Start();

        seeker = GetComponent<Seeker>();

    }

    private void HealthComponent_OnDamageTaken(DamageInfo damageInfo)
    {
        Vibration.Vibrate(5);
    }

    void UpdatePath()
    {
        if (seeker.IsDone())
        {
            seeker.StartPath(rigidbody.position, target.position, OnPathComplite);
        }
    }

    private void OnPathComplite(Path p)
    {
        PathToTarget = p.vectorPath.ToArray();

    }

    private void Update()
    {
        if (healthComponent.IsDead)
            return;
        UpdateAppearance();
        if(IsUsePathFinding)
        {
            UpdatePath();
            if (PathToTarget == null)
            {
                closestPoint = -1;
                return;
            }
            //closestPoint = -1;
            float minDistance = 1000;
            for (int i = 1; i < PathToTarget.Length; i++)
            {
                var dist = (transform.position - PathToTarget[i]).magnitude;
                if (dist < minDistance)
                {
                    closestPoint = i;
                    minDistance = dist;
                }
            }
            if (closestPoint >= 0)
            {
                ShoudDrawPoint = closestPoint >= 0;
                DrawPoint = PathToTarget[closestPoint];
            }
        }
    }

    void FixedUpdate()
    {
        pursuitTimer.Update(Time.fixedDeltaTime * 1000f);
        randomFlipTimer.Update(Time.fixedDeltaTime * 1000f);

        if (healthComponent.IsDead)
            return;

        if (target != null)
        {

            // Get vector to target
            Vector2 diffPosition = ((Vector2)target.position - rigidbody.position);
            float triggerDiffXValue = 0.5f;
            float triggerDiffYValue = 3f;

            // Get vector to closest point on Path
            if (IsUsePathFinding && PathToTarget != null)
            {
                int i = Math.Min(closestPoint + 1, PathToTarget.Length - 1);
                Vector3 pos = PathToTarget[i];
                diffPosition = ((Vector2)pos - (Vector2)transform.position);
                triggerDiffXValue = 0.01f;             // just realy close to 0
                triggerDiffYValue = 0.4f;
            }


            if (isPursuit)
            {
                if (Mathf.Abs(diffPosition.x) < triggerDiffXValue)
                    walkXDirrection = 0;
                else
                    walkXDirrection = Mathf.Sign(diffPosition.x);

                if (Mathf.Abs(diffPosition.y) > triggerDiffYValue)
                {
                    rigidbody.gravityScale = -Mathf.Sign(diffPosition.y);
                }

                IsUsePathFinding = IsTargetBehindeObstacle();
            }
        }


        var newVelocity = new Vector2(walkXDirrection * CurrentSpeed * Time.fixedDeltaTime, rigidbody.velocity.y);
        rigidbody.velocity = newVelocity;
        var hit = Physics2D.Raycast(rigidbody.position, new Vector2(walkXDirrection, 0), SeeDistance, LayerMask.GetMask(PursuitTag));
        if (hit.collider != null)
        {
            target = hit.collider.transform;
            isPursuit = true;
            pursuitTimer.Start();
        }
    }

    private bool IsTargetBehindeObstacle()
    {
        var dir = ((Vector2)target.position - rigidbody.position).normalized;
        var hit = Physics2D.Raycast(rigidbody.position, dir, 50, RaycastLayerMask);
        var isTargetBehindeObstacles = true;

        if (hit.collider != null)
        {
            isTargetBehindeObstacles = !(hit.collider.transform == target); // just check if between us and target some wall
        }
        return isTargetBehindeObstacles;
    }
    private void HealthComponent_OnDie(GameObject self, GameObject killer)
    {
        Destroy(gameObject);
    }

    private void UpdateAppearance()
    {
        var newScale = rigidbody.transform.localScale;
        if (IsUpsideDown)
            newScale.y = -Mathf.Abs(rigidbody.transform.localScale.y);
        else
            newScale.y = Mathf.Abs(rigidbody.transform.localScale.y);

        if (IsTurningLeft)
            newScale.x = -Mathf.Abs(rigidbody.transform.localScale.x);
        else
            newScale.x = Mathf.Abs(rigidbody.transform.localScale.x);

        rigidbody.transform.localScale = newScale;

    }

    private void PursuitTimer_OnCountDown1()
    {
        randomFlipTimer.Start();
        if (!isPursuit)
        {
            if (new System.Random().Next(100) < 12)   // 12% chance of changing flip dirrection
            {
                rigidbody.gravityScale *= -1;
            }
            if (new System.Random().Next(100) < 12)   // 12% chance of changing flip dirrection
            {
                walkXDirrection *= -1;
            }
        }
    }

    private void PursuitTimer_OnCountDown()
    {
        isPursuit = false;
        IsUsePathFinding = false;
        walkXDirrection = new System.Random().Next(0, 2) == 0 ? -1 : 1;
        Debug.Log("OnCountDown");
    }
    // Update is called once per frame

    private void OnCollisionEnter2D(Collision2D collision)
    {
        GameObject go = collision.gameObject;
        if (go.tag.Equals("Player"))
        {
            go.GetComponent<HealthComponent>().Kill(gameObject);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        walkXDirrection = -walkXDirrection;
    }

    private void OnDrawGizmos()
    {
       if(ShoudDrawPoint)
        {
            Gizmos.DrawSphere(DrawPoint, 0.3f);
        }
    }
}
