using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityListener : MonoBehaviour
{
    public float GravityScale = 1;
    
    private Rigidbody2D _rigidBody;
    private WorldController _worldController;


    void Start()
    {
        _rigidBody = gameObject.GetComponent<Rigidbody2D>();
        _worldController = GameObject.Find("WorldController").GetComponent<WorldController>();
        _worldController.OnGravityChanged += OnGravityChanged;
    }

    private void OnGravityChanged(float gravityDirection)
    {
        _rigidBody.gravityScale = gravityDirection * GravityScale;
    }
}
