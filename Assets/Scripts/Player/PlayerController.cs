using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Animator Animator;
    public bool IsUpsideDown { get => _rigidBody?.gravityScale < 0; }
    public CustomInputManager InputManager;

    public float Speed = 150;
    public float GravityScale = 2;
    public AudioClip DeathAuidio;
    public GUIParent GUIController;

    private Rigidbody2D _rigidBody;
    private Vector2 _moveVector;
    private WorldController _worldController;
    private WeaponComponent _weaponComponent;
    private bool _isTurnLeft = false;
    private bool _isGrounded;
    private bool _isMenuOpened = false;
    private HealthComponent _healthComponent;
    void Start()
    {
        Vibration.Init();
        Application.targetFrameRate = 120;
        _rigidBody = gameObject.GetComponent<Rigidbody2D>();
        _weaponComponent = gameObject.GetComponent<WeaponComponent>();
        _worldController = GameObject.Find("WorldController").GetComponent<WorldController>();
        _worldController.OnGravityChanged += OnGravityChanged;
        _healthComponent = gameObject.GetComponent<HealthComponent>();
        _healthComponent.OnDamageTaken += OnDamageTaken;
        _healthComponent.OnDie += OnDie;
        InputManager.OnJump += OnJump;
        InputManager.OnJumpToggle += OnJumpToggle;
        InputManager.OnMove += OnMove;
        InputManager.OnFire += OnFire;

    }

  
    private void OnJump(float direction)
    {
        if(Mathf.Sign(_rigidBody.gravityScale) != -direction)
            Vibration.Vibrate(5);
        OnGravityChanged(-direction);
    }
    private void OnJumpToggle()
    {
        _worldController.RequestChangeGravity();
    }
    private void OnMove(float direction)
    {
        _moveVector.x = direction;
        if (_moveVector.x < 0)
            _isTurnLeft = true;
        else
            _isTurnLeft = false;
    }
    private void OnFire(Vector2 direction)
    {
        if(!_healthComponent.IsDead)
            _weaponComponent.Shoot(direction.normalized); 
    }

    void Update()
    {
        if (_healthComponent.IsDead)
            return;

        if(Input.GetKeyDown(KeyCode.Escape))
        {
            _isMenuOpened = !_isMenuOpened;
            GUIController.ActivateGUI(_isMenuOpened ? "PauseMenu" : "HUD");
        }
   
        changeHorizontalAppeariance();
       
       

        Animator.SetFloat("speed", Mathf.Abs(_moveVector.x));
    }

   

    private void FixedUpdate()
    {

        if (_healthComponent.IsDead)
            return;
        float maxYVelocity = 20;
        var newVelocity = new Vector2(_moveVector.x * Speed * Time.fixedDeltaTime, _rigidBody.velocity.y);
        newVelocity.y = Mathf.Clamp(newVelocity.y, -maxYVelocity, maxYVelocity);
        _rigidBody.velocity = newVelocity;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _isGrounded = true;
        Animator.SetTrigger("isGroundedTrigger");
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        _isGrounded = false;
    }


    private void OnGravityChanged(float gravityDirection)
    {
        _rigidBody.gravityScale = gravityDirection * GravityScale;
        changeVerticalAppeariance();
        Animator.SetTrigger("jumpTrigger");
        // play here some effect
    }

    private void changeVerticalAppeariance()
    {
        var scale = transform.localScale;
        if (IsUpsideDown)
            scale.y = -Mathf.Abs(scale.y);
        else
            scale.y = Mathf.Abs(scale.y);

        transform.localScale = scale;
    }
    private void changeHorizontalAppeariance()
    {
        var scale = transform.localScale;
        if (_isTurnLeft)
            scale.x = -Mathf.Abs(scale.x);
        else
            scale.x = Mathf.Abs(scale.x);

        transform.localScale = scale;
    }
    private void OnDie(GameObject self, GameObject killer)
    {
        _rigidBody.velocity = Vector2.zero;
        _rigidBody.gravityScale = 0;
        Animator.SetTrigger("dieTrigger");
        AudioSource.PlayClipAtPoint(DeathAuidio, transform.position,4f);

        GUIController.ActivateGUI("HUD");
        GUIController.ActivateNoHide("GameOver");
    }

    private void OnDamageTaken(DamageInfo damageInfo)
    {
    }

}
