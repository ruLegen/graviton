using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectTileComponent : MonoBehaviour
{
    // Start is called before the first frame update
    public float InitialSpeed = 100f;
    public float Damage = 100f;
    public Vector2 ShootVector;
    public bool Initialized = true;
    public GameObject DestroyEffect;
    public AudioClip[] ShootAudioClip;
    public AudioClip[] DestroyAudioClip;

    private Rigidbody2D rigidbody;
    private AudioSource audioSource;
    void Start()
    {
        audioSource = gameObject.GetComponent<AudioSource>();
        rigidbody = gameObject.GetComponent<Rigidbody2D>();
        rigidbody.velocity = ShootVector * InitialSpeed;
        Initialized = true;
        AudioSource.PlayClipAtPoint(GetRandomSound(ShootAudioClip), transform.position);
    }



    public void Init(float speed, Vector2 vector,float damage)
    {
        InitialSpeed = speed;
        ShootVector = vector;
        Damage = damage;
    }
    private AudioClip GetRandomSound(AudioClip[] clips)
    {
        if (clips == null)
            return null;
        int rand = new System.Random().Next(0, clips.Length);
        return clips[rand];
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if(rigidbody.velocity.magnitude < InitialSpeed*0.1 && Initialized)
        {
            DestroyProjectTile();
        }
    }

    private void DestroyProjectTile()
    {
        AudioSource.PlayClipAtPoint(GetRandomSound(DestroyAudioClip), transform.position);

        var particle = GameObject.Instantiate(DestroyEffect, transform.position, Quaternion.identity);
        Destroy(particle,1f);

        Destroy(gameObject);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        var healthCompnent = collision.gameObject.GetComponent<HealthComponent>();
        if (healthCompnent == null)
            return;
        healthCompnent.ApplyDamage(new DamageInfo(gameObject, Damage));
        DestroyProjectTile();
    }
}
