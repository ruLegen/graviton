using Pathfinding;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPositionChanger : MonoBehaviour
{
    public GameObject PlayerObject;
    public bool IsSpawnOnGraphScaned = true;
    private GraphNode[] walkableNodes;

    // Start is called before the first frame update
    void Start()
    {
        AstarPath.OnLatePostScan += OnGraphScanned;
    }


    private void OnGraphScanned(AstarPath script)
    {
        InitNodes(script);
        SpawnPlayer();
    }

    private void InitNodes(AstarPath script)
    {
        List<GraphNode> nodes = new List<GraphNode>();
        script.data.gridGraph.GetNodes((n) =>
        {
            if (n.Walkable)
                nodes.Add(n);
        });
        walkableNodes = nodes.ToArray();
    }

    private void SpawnPlayer()
    {
        int index = new System.Random((int)Time.realtimeSinceStartup).Next(0, walkableNodes.Length);
        var spawnNode = walkableNodes[index];
        var spawnPosition = spawnNode.position;
        if (!IsSpawnOnGraphScaned)
            spawnPosition = new Int3(0,0,0);
        PlayerObject.transform.position = (Vector3)spawnPosition;
        walkableNodes = null;
    }

}
