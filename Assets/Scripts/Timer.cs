﻿using System;

public class Timer
{
    public event Action OnCountDown;

    public TimeSpan RemainTime { get => currentTime; }
    private TimeSpan totalTime;
    private TimeSpan currentTime;

    private bool isStarted = false;
    public Timer(TimeSpan totalTime)
    {
        this.totalTime = totalTime;
        this.currentTime = TimeSpan.FromSeconds(0);
    }
    public void Start()
    {
        currentTime = totalTime;
        isStarted = true;
    }
    public void Update(float ms)
    {
        if (currentTime.TotalMilliseconds <= 0)
            return;

        if(isStarted)
        {
            currentTime = currentTime.Subtract(TimeSpan.FromMilliseconds(ms));
            if (currentTime.TotalMilliseconds <= 0)
                OnCountDown?.Invoke();
        }
    }

    internal void Stop()
    {
        currentTime = TimeSpan.FromSeconds(0);
        isStarted = false;
    }
}
