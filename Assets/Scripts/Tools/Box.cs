using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Graviton.Utils.NormalDistributionRandom;

namespace Graviton.Utils
{
    public enum SplitDirection
    {
        HORIZONTAL,
        VERTICAl
    }
    public class Box
    {
        public Vector2 Center
        {
            get => new Vector2(origin.x + width / 2, origin.y + height / 2);
        }
        public LineSegment[] lines;

        private Vector2 origin;
        private float width, height;
        private Vector2[] verticies;

        public Box(Vector2 Origin, float Width, float Height)
        {
            origin = Origin;
            width = Width;
            height = Height;
            initVerticies();
            initLines();
        }

        private void initVerticies()
        {
            verticies = new Vector2[4];
            verticies[0] = origin;
            verticies[1] = origin + new Vector2(width, 0);
            verticies[2] = origin + new Vector2(width, height);
            verticies[3] = origin + new Vector2(0, height);
        }

        private void initLines()
        {
            lines = new LineSegment[4];
            lines[0] = new LineSegment(verticies[0], verticies[1]);
            lines[1] = new LineSegment(verticies[1], verticies[2]);
            lines[2] = new LineSegment(verticies[2], verticies[3]);
            lines[3] = new LineSegment(verticies[3], verticies[0]);
        }

        public float GetDistance(Box otherBox)
        {
            return Math.Abs(origin.x - otherBox.origin.x) + Math.Abs(origin.y - otherBox.origin.y);
        }


        public static bool IsBoxesIntersect(Box a, Box b)
        {
            var AxMin = a.origin.x;
            var AyMin = a.origin.y;
            var AxMax = a.origin.x + a.width;
            var AyMax = a.origin.y + a.height;
            var BxMin = b.origin.x;
            var ByMin = b.origin.y;
            var BxMax = b.origin.x + b.width;
            var ByMax = b.origin.y + b.height;
            return (AxMin <= BxMax && AxMax >= BxMin) && (AyMin <= ByMax && AyMax >= ByMin);
        }
        public static Vector2 GetRandomPointInside(Box box)
        {
            var minX = box.origin.x;
            var maxX = minX + box.width;
            var minY = box.origin.y;
            var maxY = minY + box.height;
            return new Vector2(GetRand(minX, maxX), GetRand(minY, maxY));
        }

        public static Box[] Split(Box box, SplitDirection orientation, Vector2 point)
        {
            var boxes = new Box[2];

            var w1 = point.x - box.origin.x;
            var w2 = box.width - w1;

            var h1 = point.y - box.origin.y;
            var h2 = box.height - h1;

            if (orientation == SplitDirection.VERTICAl)
            {
                boxes[0] = new Box(box.origin, w1, box.height);
                boxes[1] = new Box(new Vector2(point.x, box.origin.y), w2, box.height);
            }
            else
            {
                boxes[0] = new Box(box.origin, box.width, h1);
                boxes[1] = new Box(new Vector2(box.origin.x, point.y), box.width, h2);
            }
            return boxes;
        }


    }
}


