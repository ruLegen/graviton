﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graviton.Utils
{
    public class BoxGraphNode
    {
        public Box Root;
        public Edge[] Edges;

        public BoxGraphNode(Box root, Edge[] edges)
        {
            Root = root;
            Edges = edges;
        }
    }
}
