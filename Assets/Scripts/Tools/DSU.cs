using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Graviton.Utils
{

    //TODO Make Generic
    public class DSU
    {
        Dictionary<Box, Box> p = new Dictionary<Box, Box>();
        Dictionary<Box, int> rank = new Dictionary<Box, int>();


        public void MakeSet(Box x)
        {
            p[x] = x;
            rank[x] = 0;
        }

        public Box Find(Box x)
        {
            return (x == p[x] ? x : Find(p[x]));
        }

        public void Union(Box x, Box y)
        {
            if ((x = Find(x)) == (y = Find(y)))
                return;

            if (rank[x] < rank[y])
                p[x] = y;
            else
            {
                p[y] = x;
                if (rank[x] == rank[y])
                {
                    var r = rank[x];
                    rank[x] = ++r;
                }
            }
        }
    }
}
