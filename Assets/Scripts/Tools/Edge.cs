﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graviton.Utils
{
    public class Edge
    {
        public Box Start;
        public Box Stop;
        public float Weight;

        public LineSegment EdgeLine
        {
            get => new LineSegment(Start.Center, Stop.Center);
        }
        public Edge(Box start, Box stop, float weight)
        {
            Start = start;
            Stop = stop;
            Weight = weight;
        }

        // Check if 2 edges connected to the same Nodes
        public static bool IsSameEdge(Edge edge1, Edge edge2)
        {
            var a = edge1.Start == edge2.Start && edge1.Stop == edge2.Stop;
            var b = edge1.Start == edge2.Stop && edge1.Stop == edge2.Start;
            return a || b;
        }
    }
}
