﻿using UnityEngine;

namespace Graviton.Utils
{
    public class LineSegment
    {
        public Vector2 Start;
        public Vector2 Stop;

        public LineSegment(Vector2 start, Vector2 stop)
        {
            this.Start = start;
            this.Stop = stop;
        }
        public float Width
        {
            get 
            { 
                var width = Mathf.Abs(Start.x - Stop.x);
                return width == 0 ? 1 : width;
            }
        }
        public float Height
        {
            get
            {
                var height = Mathf.Abs(Start.y - Stop.y);
                return height == 0 ? 1 : height;
            }
        }
        // TODO Simplify
        public bool IsIntersect(LineSegment otherLine)
        {
            var a = Start.x;
            var b = Start.y;
            var c = Stop.x;
            var d = Stop.y;
            var p = otherLine.Start.x;
            var q = otherLine.Start.y;
            var r = otherLine.Stop.x;
            var s = otherLine.Stop.y;
            //ab - cd line first
            //pq - rs line second
            float det, gamma, lambda;
            det = (c - a) * (s - q) - (r - p) * (d - b);
            if (det == 0)
            {
                return false;
            }
            else
            {
                lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det;
                gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det;
                return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
            }
        }
    }
}
