﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graviton.Utils
{
    public class NormalDistributionRandom
    {
        static Random  random = new Random();

        public static float GetRand(float min, float max)
        {
            return randn_bm() * (max - min) + min;
        }
        private static float randn_bm()
        {
            var timeStamp = (DateTime.Now - new DateTime(0)).TotalSeconds;

            float u = 0, v = 0;
            while (u == 0) u = (float)random.NextDouble();              //Converting [0,1) to (0,1)
            while (v == 0) v = (float)random.NextDouble(); 

            var num = Math.Sqrt(-2.0 * Math.Log(u)) * Math.Cos(2.0 * Math.PI * v);
            num = num / 10.0 + 0.5;                                     // Translate to 0 -> 1
            if (num > 1 || num < 0) return randn_bm();                  // resample between 0 and 1
            
            return (float)num;
        }
    }
}
