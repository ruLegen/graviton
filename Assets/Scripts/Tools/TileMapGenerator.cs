using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using Graviton.Utils;
using static Graviton.Utils.NormalDistributionRandom;
using System;
using System.Linq;
using UnityEngine.Events;

public class TileMapGenerator : MonoBehaviour
{
    public event UnityAction TileGenerated;
    public int width = 100;
    public int height = 100;
    private Vector3Int startPosition;
    public int depth = 3;
    Box RootBox;
    Box[] SplitedBoxes;
   
    public Tile DrawTile;
    public Tilemap DrawTileMap;
    void Start()
    {
        var currentBounds = DrawTileMap.cellBounds;
        width = currentBounds.size.x;
        height = currentBounds.size.y;
        startPosition = currentBounds.position;
        Generate();

    }



    public void Generate()
    {
        
        RootBox = new Box(new Vector2(0, 0), width, height);
        SplitedBoxes = Box.Split(RootBox, SplitDirection.HORIZONTAL, Box.GetRandomPointInside(RootBox));
        Queue<Box> queue = new Queue<Box>();
        queue.Enqueue(SplitedBoxes[0]);
        queue.Enqueue(SplitedBoxes[1]);

        for (int i = 0; i < depth; i++)
        {
            int currentQueueSize = queue.Count;
            for (int j = 0; j < currentQueueSize; j++)
            {
                var currentBox = queue.Peek();
                queue.Dequeue();

                var direction = NormalDistributionRandom.GetRand(0, 100) < GetRand(0, 100) ? SplitDirection.HORIZONTAL : SplitDirection.VERTICAl;
                var splited = Box.Split(currentBox, direction, Box.GetRandomPointInside(currentBox));
                foreach (var splitedBox in splited)
                {
                    queue.Enqueue(splitedBox);
                }
            }
        }
        SplitedBoxes = queue.ToArray();

        List<BoxGraphNode> leafStruct = new List<BoxGraphNode>();
        List<Edge> allEdges = new List<Edge>();
        for (int i = 0; i < SplitedBoxes.Length; i++)
        {
            var currBox = SplitedBoxes[i];
            List<Edge> neighbours = new List<Edge>();
            for (int j = 0; j < SplitedBoxes.Length; j++)
            {
                var inBox = SplitedBoxes[j];
                if (currBox != inBox)
                {
                    if (Box.IsBoxesIntersect(currBox, inBox))
                    {
                        var edge = new Edge(currBox, inBox, currBox.GetDistance(inBox));
                        neighbours.Add(edge);
                        allEdges.Add(edge);
                    }
                }
            }
            leafStruct.Add(new BoxGraphNode(currBox, neighbours.ToArray()));
        }


        var dsu = new DSU();
        foreach (var b in SplitedBoxes)
        {
            dsu.MakeSet(b);
        }

        List<Edge> uniqEdges = new List<Edge>();
        foreach (var edge in allEdges)
        {
            bool found = false;
            for (int i = 0; i < uniqEdges.Count; i++)
            {
                var uniqEdge = uniqEdges[i];
                if (Edge.IsSameEdge(uniqEdge, edge))
                {
                    found = true;
                    break;
                }
            }
            if (!found)
                uniqEdges.Add(edge);
        }
        // Sort edges by weight  in non-decreasing order
        uniqEdges.Sort((Edge a, Edge b) =>
        {
            return (int)(a.Weight - b.Weight);
        });
        List<Edge> resultGraph = new List<Edge>();
        foreach (var edge in uniqEdges)
        {
            var x = dsu.Find(edge.Start);
            var y = dsu.Find(edge.Stop);
            // in result and increment the index of result for next edge
            if (x != y)
            {
                resultGraph.Add(edge);
                dsu.Union(x, y);
            }
        }
        foreach (var b in SplitedBoxes)
        {
            foreach (var edge in resultGraph)
            {
                var graphLine = edge.EdgeLine;
                b.lines = b.lines.Where(boxLine => !boxLine.IsIntersect(graphLine)).ToArray();
            }
        }
        DrawOnTilemap();
        DrawTileMap.GetComponent<TilemapCollider2D>().ProcessTilemapChanges();      // Force Generate collider 
        TileGenerated?.Invoke();
        Debug.LogWarning("Generated");

    }
    private void DrawOnTilemap()
    {
        DrawTileMap.ClearAllTiles();
        foreach (var b in SplitedBoxes)
        {
            foreach (var line in b.lines)
            {
                DrawLine(line.Start, line.Stop);
            }
        }
    }

    // Bresenham Line Algorythm 
    private void DrawLine(Vector2 start, Vector2 stop)
    {
        int x = (int)start.x;
        int y = (int)start.y;
        int x2 = (int)stop.x;
        int y2 = (int)stop.y;

        int w = x2 - x;
        int h = y2 - y;
        int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
        if (w < 0) dx1 = -1; else if (w > 0) dx1 = 1;
        if (h < 0) dy1 = -1; else if (h > 0) dy1 = 1;
        if (w < 0) dx2 = -1; else if (w > 0) dx2 = 1;
        int longest = Math.Abs(w);
        int shortest = Math.Abs(h);
        if (!(longest > shortest))
        {
            longest = Math.Abs(h);
            shortest = Math.Abs(w);
            if (h < 0) dy2 = -1; else if (h > 0) dy2 = 1;
            dx2 = 0;
        }
        int numerator = longest >> 1;
        for (int i = 0; i <= longest; i++)
        {
            SetTile(x, y);
            numerator += shortest;
            if (!(numerator < longest))
            {
                numerator -= longest;
                x += dx1;
                y += dy1;
            }
            else
            {
                x += dx2;
                y += dy2;
            }
        }
    }

    private void SetTile(int x, int y)
    {
        var pos = startPosition + new Vector3Int(x, y, 0);
        DrawTileMap.SetTile(pos, DrawTile);
    }

}
