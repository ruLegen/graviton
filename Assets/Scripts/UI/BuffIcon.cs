using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuffIcon : MonoBehaviour
{
    public Sprite Icon;

    public Image IconImage;
    public Text TimeText;
    
    public TimeSpan CurrentTime
    {
        get => currentTime;
        set
        {
            currentTime = value;
            TimeText.text = currentTime.ToString(@"mm\:ss");
        }
    }

    public TimeSpan MaxTime
    {
        get => maxTime;
        set
        {
            maxTime = value;
        }
    }
    [SerializeField] private TimeSpan currentTime;
    [SerializeField] private TimeSpan maxTime;


    void Start()
    {
        IconImage.sprite = Icon;
        MaxTime = maxTime;
        CurrentTime = maxTime;
    }

    // Update is called once per frame
    void Update()
    {
    }
}
