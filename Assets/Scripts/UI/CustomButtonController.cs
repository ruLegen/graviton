using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class ClickEvent : UnityEvent { }
public class CustomButtonController : MonoBehaviour
{
    public float WaitTime = 1f;
    public AudioClip LoadingCLip;
    public AudioClip LoadedClip;
    public ClickEvent clickEvent;
    
    private Timer timer;
    private SpriteRenderer sprite;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = LoadingCLip;
        sprite = GetComponent<SpriteRenderer>();
        timer = new Timer(System.TimeSpan.FromSeconds(WaitTime));
        timer.OnCountDown += Timer_OnCountDown;
    }

    private void Timer_OnCountDown()
    {
        Debug.Log("Activated");
        audioSource.clip = LoadedClip;
        audioSource.Play();

        clickEvent?.Invoke();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        timer.Update(Time.fixedDeltaTime* 1000f);
        float val = (float)timer.RemainTime.TotalSeconds;

        if (val != 0)
            sprite.material.SetFloat("val", 1- (val/WaitTime));
        else
            sprite.material.SetFloat("val", 0);

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        timer.Start();
        audioSource.clip = LoadingCLip;
        audioSource.Play();
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        timer.Stop();
        audioSource.Stop();
    }
    void OnDestroy()
    {
        timer.OnCountDown -= Timer_OnCountDown;
    }
}
