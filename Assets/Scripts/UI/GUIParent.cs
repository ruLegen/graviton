using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIParent : MonoBehaviour
{
    public string DefaultTag = "GUI";
    private GameObject[] Children;
    void Start()
    {
        int id = 0;
        List<GameObject> children = new List<GameObject>();
        foreach(Transform child in transform)
        {
            var obj = child.gameObject;
            var guicomponent = obj.GetComponent<GUIChild>();
            if(guicomponent == null)
            {
                guicomponent = obj.AddComponent<GUIChild>();
                guicomponent.ID = id;
                guicomponent.Tag = DefaultTag+id;

            }
            children.Add(obj);
            id++;
        }
        Children = children.ToArray();
    }

    public void ActivateGUI(int id)
    {
        GameObject activatingGUI = FindGUIByID(id);
        if (activatingGUI != null)
        {
            HideAll();
            activatingGUI.SetActive(true);
        }
        else
        {
            Debug.LogError("Cannot fild GUIChild with ID=" + id);
        }
    }
    public void ActivateGUI(string tag)
    {
        GameObject activatingGUI = FindGUIByTag(tag);
        if (activatingGUI != null)
        {
            HideAll();
            activatingGUI.SetActive(true);
        }
        else
        {
            Debug.LogError("Cannot fild GUIChild with Tag=" + tag);
        }
    }


    public void ActivateNoHide(int id)
    {
        GameObject activatingGUI = FindGUIByID(id);
        if (activatingGUI != null)
        {
            activatingGUI.SetActive(true);
        }
        else
        {
            Debug.LogError("Cannot fild GUIChild with ID=" + id);
        }
    }
    public void ActivateNoHide(string tag)
    {
        GameObject activatingGUI = FindGUIByTag(tag);
        if (activatingGUI != null)
        {
            activatingGUI.SetActive(true);
        }
        else
        {
            Debug.LogError("Cannot fild GUIChild with Tag=" + tag);
        }
    }
    private GameObject FindGUIByTag(string tag)
    {
        GameObject activatingGUI = null;
        for (int i = 0; i < Children.Length; i++)
        {
            if (Children[i].GetComponent<GUIChild>().Tag == tag)
            {
                activatingGUI = Children[i];
                break;
            }
        }

        return activatingGUI;
    }
    private GameObject FindGUIByID(int id)
    {
        GameObject activatingGUI = null;
        for (int i = 0; i < Children.Length; i++)
        {
            if (Children[i].GetComponent<GUIChild>().ID == id)
            {
                activatingGUI = Children[i];
                break;
            }
        }

        return activatingGUI;
    }

    public void HideAll()
    {
        foreach (var child in Children)
        {
            child.SetActive(false);
        }
    }
}
