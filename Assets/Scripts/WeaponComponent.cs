using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponComponent : MonoBehaviour
{
    public float ShootDelay
    {
        get => _shootDelay;
        set
        {
            _shootDelay = value < 0 ? 0:value;
            InitTimer();
        }
    }


    public GameObject ProjectTile;
    public float ProjectTileVelocity = 100f;
    public float ProjectTileDamage = 30;
    private Timer shootCooldownTimer;
    private bool canShoot = true;


    [SerializeField]
    private float _shootDelay = 1f;
    void Start()
    {
        InitTimer();
    }

    void InitTimer()
    {
        shootCooldownTimer = new Timer(TimeSpan.FromSeconds(ShootDelay));
        shootCooldownTimer.OnCountDown += () => { canShoot = true; };
        shootCooldownTimer.Start();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        shootCooldownTimer.Update(Time.fixedDeltaTime * 1000f);
    }
    public void Shoot( Vector2 shootVector)
    {
        if (!canShoot) return;

        canShoot = false;
        var projecttile = GameObject.Instantiate(ProjectTile, (Vector2)transform.position, Quaternion.identity);

        var component = projecttile.GetComponent<ProjectTileComponent>();
        component.Init(ProjectTileVelocity, shootVector, ProjectTileDamage);

        Physics2D.IgnoreCollision(projecttile.GetComponentInChildren<CircleCollider2D>(), GetComponentInChildren<BoxCollider2D>());
        shootCooldownTimer.Start();
    }
}
