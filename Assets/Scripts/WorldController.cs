using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldController : MonoBehaviour
{
    private float _gravity = 1;

    public event Action<float> OnGravityChanged;


    // Start is called before the first frame update
    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void RequestChangeGravity()
    {
        _gravity *= -1;
        OnGravityChanged.Invoke(_gravity);
    }
}
