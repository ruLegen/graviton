class DSU{
  
  constructor(N)
  {
    this.p= new Map();
    this.rank = new Map();
  }


  MakeSet(x) 
  {
    this.p.set(x,x);
    this.rank.set(x,0);
  }

  Find(x) 
  {
    // If i is the parent of itself
    
      return ( x == this.p.get(x) ? x : this.Find(this.p.get(x)));
  }

 Union(x,  y) 
  {
      if ( (x = this.Find(x)) == (y = this.Find(y)) )
          return;
      
      if ( this.rank.get(x) <  this.rank.get(y) )
          this.p.set(x,y);
      else {
          this.p.set(y,x);
          if (this.rank.get(x) == this.rank.get(y))
            {
              let r =this.rank.get(x);
              this.rank.set(x,++r)
            }
      }
  }
}


const VERT_SPLIT = 0;
const HOR_SPLIT = 1;
var verticies =[];
function DrawBox(box,stroke)
{
  //rect(box.x,box.y,box.w,box.h);
  box.lines.forEach((lineSegment)=>{
    lineSegment.draw(stroke);
  })
}
function LineSegment(start,stop)
{
  this.start = start;
  this.stop = stop;
  this.draw = (s)=>{
    stroke(s)
    line(start.x,start.y,stop.x,stop.y)
  }
  this.IsIntersect=(otherLine)=>{
    let a = this.start.x;
    let b = this.start.y;
    let c = this.stop.x;
    let d = this.stop.y;
    let p = otherLine.start.x;
    let q = otherLine.start.y;
    let r = otherLine.stop.x;
    let s = otherLine.stop.y;
    //ab - cd line first
    //pq - rs line second
    var det, gamma, lambda;
    det = (c - a) * (s - q) - (r - p) * (d - b);
    if (det === 0) {
      return false;
    } else {
      lambda = ((s - q) * (r - a) + (p - r) * (s - b)) / det;
      gamma = ((b - d) * (r - a) + (c - a) * (s - b)) / det;
      return (0 < lambda && lambda < 1) && (0 < gamma && gamma < 1);
    }
  }
}
class Box
{
  constructor(x,y,w,h)
  {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.lines = this.generateLines();
  }
  generateLines(){
    let lines=[];
    let v1 = createVector(this.x,this.y)
    let v2 = createVector(this.x+this.w,this.y)
    let v3 = createVector(this.x,this.y+this.h)
    let v4 = createVector(this.x+this.w,this.y+this.h)
    
    lines.push(new LineSegment(v1,v2))
    lines.push(new LineSegment(v1,v3))
    lines.push(new LineSegment(v2,v4))
    lines.push(new LineSegment(v3,v4))
    return lines
    
  }
  center() { 
      return createVector(this.x+this.w/2,this.y+this.h/2)
  }
  
  distance(otherBox){
    return Math.abs(this.x-otherBox.x) + Math.abs(this.y-otherBox.y)
  }
}
function Color(r,g,b)
{
  this.r = r;
  this.g = g;
  this.b = b;
}
function Split(box,orientation,point)
{
  let boxes = [];
  if(orientation == VERT_SPLIT)
  {
      let w1 = point.x - box.x;
      let w2 = box.w - w1;
      boxes.push(new Box(box.x,box.y,w1,box.h))
      boxes.push(new Box(point.x,box.y,w2,box.h))
  }else
  {
      let h1 = point.y - box.y;
      let h2 = box.h - h1;
      boxes.push(new Box(box.x,box.y,box.w,h1))
      boxes.push(new Box(box.x,point.y,box.w,h2))
  }
  return boxes;
}
function getRand(min, max) {
  return randn_bm() * (max - min) + min;
}
function randn_bm() {
  let u = 0, v = 0;
  while(u === 0) u = Math.random(); //Converting [0,1) to (0,1)
  while(v === 0) v = Math.random();
  let num = Math.sqrt( -2.0 * Math.log( u ) ) * Math.cos( 2.0 * Math.PI * v );
  num = num / 10.0 + 0.5; // Translate to 0 -> 1
  if (num > 1 || num < 0) return randn_bm() // resample between 0 and 1
  return num
}
function RandPointInBox(box){
  let minX = box.x;
  let maxX = minX + box.w;
  let minY = box.y;
  let maxY = minY + box.h;
  return createVector(getRand(minX,maxX),getRand(minY,maxY))
}
function LeafStruct(box,edges)
{
  this.box = box;
  this.edges = edges
}
function Edge(start,stop, weight)
{
  this.start = start;
  this.stop = stop;
  this.weight = weight
  this.getLine =()=>{
    return new LineSegment(start.center(),stop.center())
  }
 this.getReverseLine =()=>{
    return new LineSegment(stop.center(),start.center())
  }
  
}

function DoBoxesIntersect( a,  b) {
  let AxMin = a.x
  let AyMin = a.y
  let AxMax = a.x+a.w
  let AyMax = a.y+a.h
  let BxMin = b.x
  let ByMin = b.y
  let BxMax = b.x+b.w
  let ByMax = b.y+b.h
  return (AxMin <= BxMax && AxMax >= BxMin) && (AyMin <= ByMax && AyMax >= ByMin   )
}
function doesPointCollide(p,box) {
    return !(p.x < box.x || p.x > box.x+box.w || p.y > box.y+box.h || p.y < box.y)
}
function VertIsEqual(edge1,edge2)
{
  let a = edge1.start == edge2.start && edge1.stop == edge2.stop
  let b = edge1.start == edge2.stop && edge1.stop == edge2.start
  return a || b;
}
var boxes =[];
var b ;
var depth = 3;
var verticies = [];
var delaunay;
var tris;
var leafStruct=[]
var dsu;
var uniqEdges = [];

var resultGraph = []
var lines=[];

function setup() {
  rectMode(CORNER);
  createCanvas(512, 512);
  b = new Box(0,0,width,height)
  boxes = Split(b,HOR_SPLIT,RandPointInBox(b))
  for(let i =0; i< depth; i++)
    {
      let tmpBox = [];
       boxes.forEach((e,i)=>{
		   let isGood = false;
		   let direction=0;
		   var splited;
		   
		   while(!isGood)
		   {
				direction = getRand(0,100) < getRand(0,100) ?HOR_SPLIT:VERT_SPLIT;
				splited = Split(e,direction,RandPointInBox(e))
				let min = Math.min(splited[0].w,splited[0].h)
				let max = Math.max(splited[0].w,splited[0].h)
				let ratio = max/min;
				isGood = ration > 2 && ratio < 3;
		   }
		   
         tmpBox.push(...splited);
      })
      boxes = [...tmpBox]
    }
  
  boxes.forEach((b)=>{
    verticies.push(b.center())
  })
  var edges = [];
  for(i = 0; i< boxes.length; i++)
  {
    var currBox = boxes[i];
    var nei = [];
      for(j=0; j < boxes.length; j++)
        {
          var inBox = boxes[j];
          if(currBox != inBox)
            {
              if(DoBoxesIntersect(currBox,inBox))
                {
                  var edge = new Edge(currBox,inBox,currBox.distance(inBox));
                  nei.push(edge)
                  edges.push(edge) 
                }
            }
        }
    leafStruct.push (new LeafStruct(currBox,nei));
  }
  
  
  /*
     Start Kraskal algorithm 
  */
  dsu = new DSU(boxes.length)
  
  boxes.forEach((b)=>{
    dsu.MakeSet(b)
  })
  
  console.log(dsu);
  // we have bi dirrectional edges, we should eliminate 1 dirrection
  // because we have to get not oriented graph
  edges.forEach((edge)=>{
      let found = false;
      for(i=0; i< uniqEdges.length; i++)
      {
        let uniqEdge = uniqEdges[i];
        if(VertIsEqual(uniqEdge,edge))
          {
            found = true;
            break;
          }
      }
    if(!found)
      uniqEdges.push(edge)
  })
  // Sort verticies by weight  in non-decreasing order
  uniqEdges.sort((a,b)=>{
    return a.weight - b.weight
  })
  
 
  uniqEdges.forEach((edge)=>{
        let x = dsu.Find(edge.start);
        let y = dsu.Find(edge.stop);
        // in result and increment the index of result for next edge
        if ( x != y )
        {
            resultGraph.push( edge );
            dsu.Union(x, y );
        }
  })
  console.log(resultGraph)
  

     
    for(i = 0; i < 10; i++)
      {
        let start = createVector(getRand(0,width),getRand(0,height));
        let stop = createVector(getRand(0,width),getRand(0,height));
        lines[i] = new LineSegment(start,stop)
      }
}
function draw() {
  background(255);
  stroke(0);
  //verticies.forEach((v,j)=>{
  //  circle(v.x,v.y,1)
  //})
  stroke(255,0,0)
  var lbox = null;
  leafStruct.forEach((l)=>{
    if(doesPointCollide(createVector(mouseX,mouseY),l.box))
      {
        lbox = l;
        return
      }
  })
  //if(lbox != null)
  //  {
  //    lbox.edges.forEach((e)=>{
  //      DrawBox(e.stop,[255,0,0])
  //    })
  //    DrawBox(lbox.box,[0,255,0])
  //  }
  resultGraph.forEach((e)=>{
     //e.getReverseLine().draw([0,0,255])
  })
  

  boxes.forEach((b,i)=>{
   resultGraph.forEach((edge)=>{
         let graphLine = edge.getLine()
         b.lines =  b.lines.filter((boxLine)=> !boxLine.IsIntersect(graphLine))
   })
   
})
  
 
  boxes.forEach((e,i)=>{
    DrawBox(e,[0,0,0])
  })
    
    
    
}